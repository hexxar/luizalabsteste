const errorHandler = (error, msg, rejectFunction) => {
  if (error) console.error(error);
  rejectFunction({ error: msg });
};

const dependencies = { errorHandler };


const NFeTranModule = require('./modules/NFeTran')(dependencies);


module.exports = {
  NFeTran: () => NFeTranModule,
};
