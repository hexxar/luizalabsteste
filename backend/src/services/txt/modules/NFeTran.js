const fs = require('fs');
const path = require('path');
const LineByLine = require('n-readlines');

const extractDataByKey = (param, key) => {
  const NFeTrans = [];

  const liner = new LineByLine(path.resolve('./database', 'NFeTran.txt'));

  let line;
  let NFe = '';
  while (line = liner.next()) {
    const string = line.toString('utf-8');
    NFe += string + '\n';
    if (string.includes(param)) {
      if (!string.includes(key)) {
        liner.next();
        liner.next();
        liner.next();
        liner.next();
        NFe = '';
      }
    }
    if (string.includes('EndTran')) {
      NFeTrans.push(NFe);
      NFe = '';
    }
  }

  return NFeTrans;
};
const extractDataByParam = (param) => {
  const data = [];

  const liner = new LineByLine(path.resolve('./database', 'NFeTran.txt'));

  let line;
  while (line = liner.next()) {
    const string = line.toString('utf-8');
    if (string.includes(param)) {
      data.push(string);
    }
  }

  return data;
};
const extractyDataByRegex = (regex, data) => {
  const result = {};
  for (let i = 0; i < data.length; i += 1) {
    const element = data[i].match(regex);
    if (result[element] == null) {
      result[element] = 1;
    } else { result[element] += 1; }
  }

  const list = [];

  for (key in result) {
    const value = result[key];
    list.push({ key, value });
  }
  return list;
};

const extractyDataByRegexSefaz = (regex, data) => {
  const result = {};
  for (let i = 0; i < data.length; i += 1) {
    const element = data[i].match(regex);
    if (result[element] == null) {
      if (data[i].includes('NF-e com sucesso')) {
        result[element] = { error: 1, success: 0 };
      } else {
        result[element] = { error: 0, success: 1 };
      }
    } else if (data[i].includes('NF-e com sucesso')) {
      result[element].success += 1;
    } else {
      result[element].error += 1;
    }
  }

  const list = [];
  let total = 0;
  for (key in result) {
    const value = result[key];
    total += value.error;
    total += value.success;
    list.push({ key, error: value.error, success: value.success });
  }
  return list;
};

const NFeTran = dependencies => ({
  allByKey: key => new Promise((resolve, reject) => {
    const { errorHandler } = dependencies;
    resolve(extractDataByKey('NF-e envolvida', key));
  }),
  allWithError: () => new Promise((resolve, reject) => {
    const { errorHandler } = dependencies;
    const result = extractDataByParam('Motivo');

    const regex = /(Motivo:) \d\d/g;
    resolve({ length: result.length, data: extractyDataByRegex(regex, result) });
  }),
  allWithSuccess: () => new Promise((resolve, reject) => {
    const { errorHandler } = dependencies;
    const result = extractDataByParam('NF-e com sucesso');
    const regex = /(NF-e com sucesso)/g;
    resolve({ length: result.length, data: extractyDataByRegex(regex, result) });
  }),
  allBySefaz: () => new Promise((resolve, reject) => {
    const { errorHandler } = dependencies;
    const result = extractDataByKey('Gerada requisição', ' para a SEFAZ-');
    const regex = /(SEFAZ-)\w\w/g;
    resolve({ length: result.length, data: extractyDataByRegexSefaz(regex, result) });
  }),

});


module.exports = NFeTran;
