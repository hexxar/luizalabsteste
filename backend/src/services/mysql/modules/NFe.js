
const NFes = dependencies => ({
  allByKey: key => new Promise((resolve, reject) => {
    const { connection, errorHandler } = dependencies;
    const query = 'SELECT * FROM NFe WHERE NFe.Chave = ?';
    connection.query(query, key, (error, results) => {
      if (error) {
        errorHandler(error, 'Falha ao listar as Nfes', reject);
        return false;
      }
      resolve({ data: results, length: results.length });
    });
  }),
  allByDataAndType: (initial, end, type) => new Promise((resolve, reject) => {
    const { connection, errorHandler } = dependencies;
    const query = 'SELECT  * FROM NFe WHERE Tipo = ? AND Data BETWEEN ? AND ?';
    connection.query(query, [type, initial, end], (error, results) => {
      if (error) {
        errorHandler(error, 'Falha ao listar as NF-es', reject);
        return false;
      }
      resolve({ data: results, length: results.length });
    });
  }),
  allByCnpjCpf: CnpjCpf => new Promise((resolve, reject) => {
    const { connection, errorHandler } = dependencies;
    const query = 'SELECT  * FROM NFe WHERE CnpjCpf = ?';
    connection.query(query, CnpjCpf, (error, results) => {
      if (error) {
        errorHandler(error, 'Falha ao listar as NF-es', reject);
        return false;
      }
      resolve({ data: results, length: results.length });
    });
  }),
  total: (initial, end, type) => new Promise((resolve, reject) => {
    const { connection, errorHandler } = dependencies;
    const query = 'SELECT SUM(ValorTotal) as Valor_Total, SUM(ValorProd) as Valor_Prod,SUM(ValorICMS) as Valor_ICMS,SUM(ValorIPI) as Valor_IPI FROM NFe WHERE Tipo = ? AND Data BETWEEN ? AND ?';
    connection.query(query, [type, initial, end], (error, results) => {
      if (error) {
        errorHandler(error, 'Falha ao listar as NF-es', reject);
        return false;
      }
      if (results[0].Valor_Total == null) {
        resolve({ data: results[0], length: 0 });
      } else {
        resolve({ data: results[0], length: results.length });
      }
    });
  }),
  allByStatus: status => new Promise((resolve, reject) => {
    const { connection, errorHandler } = dependencies;
    const query = 'SELECT * FROM NFe WHERE Status in (?) LIMIT 5000';
    connection.query(query, [status], (error, results) => {
      if (error) {
        errorHandler(error, 'Falha ao listar as Nfes', reject);
        return false;
      }
      resolve({ data: results, length: results.length });
    });
  }),
});

module.exports = NFes;
