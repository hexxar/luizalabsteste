const mysqlServer = require('mysql');

const connection = mysqlServer.createConnection({
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
});

const errorHandler = (error, msg, rejectFunction) => {
  if (error) console.error(error);
  rejectFunction({ error: msg });
};

const dependencies = { connection, errorHandler };


const NFesModule = require('./modules/NFe')(dependencies);


module.exports = {
  NFes: () => NFesModule,
};
