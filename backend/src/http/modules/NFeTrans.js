
const txt = require('../../services/txt');

const NFeTrans = (server) => {
  const route = 'nfetran';
  server.get(`${route}/error`, async (req, res, next) => {
    try {
      res.contentType = 'json';
      res.send({ result: await txt.NFeTran().allWithError() });
    } catch (error) {
      res.send(422, error);
    }
    next();
  });

  server.get(`${route}/success`, async (req, res, next) => {
    try {
      res.contentType = 'json';
      res.send({ result: await txt.NFeTran().allWithSuccess() });
    } catch (error) {
      res.send(422, error);
    }
    next();
  });
  server.get(`${route}/sefaz`, async (req, res, next) => {
    try {
      res.contentType = 'json';
      res.send({ result: await txt.NFeTran().allBySefaz() });
    } catch (error) {
      res.send(422, error);
    }
    next();
  });
};

module.exports = NFeTrans;
