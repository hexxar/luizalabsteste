const db = require('../../services/mysql');
const txt = require('../../services/txt');

const NFes = (server) => {
  const route = 'nfe';
  server.post(`${route}/key`, async (req, res, next) => {
    try {
      const { key } = req.body;
      res.contentType = 'json';
      res.send(await db.NFes().allByKey(key));
    } catch (error) {
      res.send(422, error);
    }
    next();
  });

  server.post(`${route}/datatype`, async (req, res, next) => {
    try {
      const { initial, end, type } = req.body;
      res.contentType = 'json';
      res.send(await db.NFes().allByDataAndType(initial, end, type));
    } catch (error) {
      res.send(422, error);
    }
    next();
  });

  server.post(`${route}/cnpjcpf`, async (req, res, next) => {
    try {
      const { CnpjCpf } = req.body;
      res.contentType = 'json';
      res.send(await db.NFes().allByCnpjCpf(CnpjCpf));
    } catch (error) {
      res.send(422, error);
    }
    next();
  });

  server.post(`${route}/total`, async (req, res, next) => {
    try {
      const { initial, end, type } = req.body;
      res.contentType = 'json';
      res.send(await db.NFes().total(initial, end, type));
    } catch (error) {
      res.send(422, error);
    }
    next();
  });

  server.post(`${route}/status`, async (req, res, next) => {
    try {
      const { status } = req.body;
      res.contentType = 'json';
      res.send(await db.NFes().allByStatus(status));
    } catch (error) {
      res.send(422, error);
    }
    next();
  });

  server.post(`${route}/fiscaltrans`, async (req, res, next) => {
    try {
      const { key } = req.body;
      res.contentType = 'json';
      res.send({ fiscal: await db.NFes().allByKey(key), trans: await txt.NFeTran().allByKey(key) });
    } catch (error) {
      res.send(422, error);
    }
    next();
  });
};

module.exports = NFes;
