const NFes = require('./modules/NFes');
const NFeTrans = require('./modules/NFeTrans');

const routes = (server) => {
  NFes(server);
  NFeTrans(server);
  server.get('/', (req, res, next) => {
    res.contentType = 'json';
    res.send({ response: 'Luiza Labs Test!' });
    next();
  });
};

module.exports = routes;
