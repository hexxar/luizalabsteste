const test = require('ava');
const request = require('supertest');
require('dotenv').config();

const server = require('../server');

server.listen(12345);

require('dotenv').config();

test('GET /nfetran/error', async (t) => {
  t.plan(2);

  const res = await request(server)
    .get('/nfetran/error');
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});

test('GET /nfetran/success', async (t) => {
  t.plan(2);

  const res = await request(server)
    .get('/nfetran/success');
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});

test('GET /nfetran/sefaz', async (t) => {
  t.plan(2);

  const res = await request(server)
    .get('/nfetran/sefaz');
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});


test('POST /nfe/key', async (t) => {
  t.plan(2);

  const res = await request(server)
    .post('/nfe/key')
    .send({ key: '89958861455662550443256825625984378899008104' });
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});

test('POST /nfe/datatype', async (t) => {
  t.plan(2);

  const res = await request(server)
    .post('/nfe/datatype')
    .send({
      initial: '01/04/2017',
      end: '03/04/2017',
      type: 'S',
    });
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});


test('POST /nfe/cnpjcpf', async (t) => {
  t.plan(2);

  const res = await request(server)
    .post('/nfe/cnpjcpf')
    .send({
      CnpjCpf: 77670213,
    });
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});


test('POST /nfe/total', async (t) => {
  t.plan(2);

  const res = await request(server)
    .post('/nfe/total')
    .send({
      initial: '01/04/2017',
      end: '03/04/2017',
      type: 'S',
    });
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});


test('POST /nfe/status', async (t) => {
  t.plan(2);

  const res = await request(server)
    .post('/nfe/status')
    .send({ status: ['DENEGADO', 'CANCELADO'] });
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});

test('POST /nfe/fiscaltrans', async (t) => {
  t.plan(2);

  const res = await request(server)
    .post('/nfe/fiscaltrans')
    .send({ key: '89958861455662550443256825625984378899008104' });
  t.is(res.status, 200);
  t.is(res.header['content-type'], 'application/json');
});
