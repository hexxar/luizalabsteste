require('dotenv').config();
const test = require('ava');

const db = require('./../services/mysql');

test('MYSQL - Query All By Key', async (t) => {
  const result = await db.NFes().allByKey(89958861455662550443256825625984378899008104);
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});

test('MYSQL - Query All By Data And Type', async (t) => {
  const result = await db.NFes().allByDataAndType('01/04/2017', '03/04/2017', 'S');
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});

test('MYSQL - Query All By CnpjCpf', async (t) => {
  const result = await db.NFes().allByCnpjCpf(77670213);
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});

test('MYSQL - Query All By Status', async (t) => {
  const result = await db.NFes().allByStatus(['DENEGADO', 'CANCELADO']);
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});
