require('dotenv').config();
const test = require('ava');

const db = require('./../services/txt');

test('TXT - Query All By Key', async (t) => {
  const result = await db.NFeTran().allByKey('74363300535527229859277391968579914989338378');
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});

test('TXT - Query All With Error', async (t) => {
  const result = await db.NFeTran().allWithError();
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});

test('TXT - Query All With Success', async (t) => {
  const result = await db.NFeTran().allWithSuccess();
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});

test('TXT - Query All By Sefaz', async (t) => {
  const result = await db.NFeTran().allBySefaz();
  if (result.length > 0) { t.pass('Json não vazio.'); } else { t.fail('Json vazio.'); }
});
