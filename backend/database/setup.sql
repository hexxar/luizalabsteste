CREATE TABLE NFe (
  Data varchar(255),
  Tipo TEXT default NULL,
  CnpjCpf int default NULL,
  Numero int default NULL,
  Serie int default NULL,
  Modelo int default NULL,
  Chave varchar(255) default NULL,
  ValorTotal DECIMAL(65,2) default NULL,
  ValorProd DECIMAL(65,2) default NULL,
  ValorICMS DECIMAL(65,2) default NULL,
  ValorIPI DECIMAL(65,2) default NULL,
  Status varchar(255) default NULL
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE '/docker-entrypoint-initdb.d/NFe.txt' INTO TABLE NFe FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';
