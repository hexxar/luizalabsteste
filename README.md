**Luiza Lab Teste**
===================


## Tabela de Tópicos

- [Introdução](#Introdução)
- [Tecnologias](#Tecnologias)
- [Setup](#Setup)
- [Documentação](#Documentação)
- [Pastas](#Pastas-e-Responsabilidades)
- [Diagramas](#Diagramas)
- [Observações](#Observações)
- [Testes](#Testes)





## **Introdução**
----------
Neste repositório se encontra a solução para o desafio proposto pela  LuizaLabs,  foram utilizadas as seguintes tecnologias: [Docker](https://github.com/docker), [Angular](https://github.com/angular/angular), [MySQL](https://www.mysql.com/), [Node.JS](https://github.com/nodejs/node) e [Resitfy](https://github.com/restify/node-restify). A seguir detalharei como cada tecnologia foi empregada.

## **Tecnologias**

 1. **Docker** : tecnologia de software que fornece contêineres, fornecendo abstração e automação de virtualização de nível de sistema operacional.
 2. **MySQL**: sistema de gerenciamento de banco de dados, que utiliza a linguagem SQL como interface.
 3.  **Node.JS**: interpretador de código JavaScript que funciona do lado do servidor. O Node.JS foi escolhido por além de ser umas das linguagens preferenciais é também uma linguagem que já conheço e trabalho.
 4. **Angular**:  plataforma de aplicações web de código-fonte aberto e front-end baseado em TypeScript
 5. **Restify**: framework do Node.js  que foi otimizado para construir serviços web RESTful.
 6. **AVA**: framework de teste para Node.js.




## **Setup**

Esta seção descreve a forma de configuração da aplicação, antes de iniciar, verifique se algumas das portas a seguir estão sendo utilizadas:

 - 3456 (Servidor NodeJS)
 - 1306 (MySQL)
 - 4200 (Angular)

Todos as etapas a seguir foram executadas no terminal.

#### **Etapa 1**
Primeiramente devemos instalar o **Docker**, seguem os links para a instalação do **Docker**

 1. Docker ([Guia de instalação](https://docs.docker.com/engine/installation/))
 2. Docker-compose - 1.13.0+ ([Guia de instalação](https://docs.docker.com/compose/install/))

#### **Etapa 2**
Depois da instalação do **Docker**, devemos clonar o repositório do projeto, para isso usaremos o seguinte comando:

```sh
git clone https://ysouzas@bitbucket.org/hexxar/luizalabsteste.git
```
Após clonarmos o projeto, devemos entrar na pasta principal com o seguinte comando:
```sh
cd luizalabsteste
```
Já dentro da pasta luizalasbteste devemos executar o seguinte comando:

```sh
docker-compose up
```
Devemos aguardar o final da instalação das imagens do **Docker**, assim que todas as imagens e containers forem instaladas, acesse url http://localhost:4200 no navegador de sua preferência.


## **Documentação**

Todo o código foi feito usando o  [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript), o código principal do backend está dentro do seguinte caminho:
```sh
luizalabsteste/backend/src
```

já o do frontend:
```sh
luizalabsteste/fronted
```
Todos os arquivos `index.js` apenas expõem as funções de suas respectivas pastas para outros módulos.

O src do backend é composto por 4 pastas e suas subpastas e arquivos, sendo eles:

```js
src/
├── http
		├── modules
				├── NFes.js
				└──  NFeTrans.js
		└──  routes.js
├── server
		├── cors.js
		└──  index.js
├── services
		├── mysql
				└──  modules
						└──  NFe.js
		└──  txt
				└──  modules
						└──  NFeTran.js
├── services
		├── mysql
				└── modules
						└──  NFe.js
		└── txt
				└── modules
						└──  NFeTran.js
└── test
		├── NFe.test.js
		├── NFeTran.test.js
		└── Route.test.js


```

### **Pastas e Responsabilidades**


 - **http** : responsável por gerir as rotas da api,  o index.js desta passa expõe as rotas que podem ser encontradas dentro da pasta modules.
 - **server**: responsável por gerenciar o cors da aplicação, é também responsável por gerenciar as configurações do servidor e subir o serviço das rotas.
 - **services**:  responsável por todos os serviços da api, é possivel encontrar as consultas `sql` dentro do module `mysql` e do arquivo `NFe.js`, já as consultas no arquivo `txt` são encontradas no modulo `txt` no arquivo  `NFeTran.js`
 - **test**: responsável por todos os testes da api, nele pode ser encontrado todos os testes, as consultas `sql`, no `txt` e também os testes nas rotas, verificando os resultados das mesmas.


### **Diagramas**

![diagrama](https://scontent.fvix2-2.fna.fbcdn.net/v/t1.0-9/26733449_1574760602606452_4430564968042904117_n.jpg?oh=f4344179573841e498101bb086e3fe64&oe=5AED3660)




### **Observações**
os arquivos .env normalmente não são enviados junto com o código, porém eles foram retirados do gitignore simplesmente para agilizar a instalação.



### **Testes**
Foi utilizado a biblioteca **AVA**, para realizar os testes. Foram feitos testes nas rotas da api, como também nas consultas `sql's` ou `txt`.

Para realizar os testes primeiramente devemos entrar na pasta do backend, segue o comando:
```sh
 cd backend
```
logo depois devemos instalar os pacotes, segue o comando:
```sh
 npm install
```
devemos agora mudar o .env, isso se deve pois o docker expõem o banco de dados para fora com outra porta e host, segue a alteração:

versão docker

> MYSQL_HOST  = db
> MYSQL_PORT  = 3306

versão teste

> MYSQL_HOST  = localhost
> MYSQL_PORT  = 1306

Resultado dos testes:

![teste](https://scontent.fvix2-2.fna.fbcdn.net/v/t1.0-9/26230436_1575963212486191_1610724466287317295_n.jpg?oh=1123134e36b3cd1429535aabe1c241cf&oe=5AEA1CC5)

### **Banco de dados**
O banco de dados utilizado foi o **MySQL**, quando o container do **Docker** é configurado, utilizamos alguns comando que inserem os dados do arquivo `NFe.txt` diretamente na tabela `NFe`.

```sh
LOAD DATA LOCAL INFILE '/docker-entrypoint-initdb.d/NFe.txt' INTO TABLE NFe FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';
```

o comando faz com que cada campo separado por `;` se torne uma coluna da tabela e o `\n` determine o final da linha.



