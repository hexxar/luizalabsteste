import { Component, OnInit } from '@angular/core';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { ApiService } from '../providers/api.service';

@Component({
  selector: 'app-nfe-status',
  templateUrl: './nfe-status.component.html',
  styleUrls: ['./nfe-status.component.css']
})
export class NfeStatusComponent implements OnInit {
  public dismissibleError = true;
  public dismissibleKey = false;
  public result = [];
  public table = true;
  constructor(private _apiService: ApiService) { }
  optionsModel: string[];
  myOptions: IMultiSelectOption[];


  pesquisar() {
    if (this.optionsModel.length === 0) {
      this.dismissibleKey = true;
    } else {
      this.dismissibleKey = false;
      this._apiService.getByStatus(this.optionsModel).subscribe((response) => {
        if (response.length === 0) {
          this.dismissibleError = false;
          this.table = true;
        } else {
          this.dismissibleError = true;
          this.result = response.data;
          this.table = false;
        }
      });
    }
  }
  ngOnInit() {
    this.myOptions = [
      { id: 'AUTORIZADO', name: 'AUTORIZADO' },
      { id: 'REJEITADO', name: 'REJEITADO' },
      { id: 'DENEGADO', name: 'DENEGADO' },
      { id: 'CANCELADO', name: 'CANCELADO' },
    ];
  }
}
