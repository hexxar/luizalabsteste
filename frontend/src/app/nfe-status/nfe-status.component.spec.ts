import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfeStatusComponent } from './nfe-status.component';

describe('NfeStatusComponent', () => {
  let component: NfeStatusComponent;
  let fixture: ComponentFixture<NfeStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfeStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfeStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
