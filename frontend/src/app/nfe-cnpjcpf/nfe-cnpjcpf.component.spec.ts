import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfeCnpjcpfComponent } from './nfe-cnpjcpf.component';

describe('NfeCnpjcpfComponent', () => {
  let component: NfeCnpjcpfComponent;
  let fixture: ComponentFixture<NfeCnpjcpfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfeCnpjcpfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfeCnpjcpfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
