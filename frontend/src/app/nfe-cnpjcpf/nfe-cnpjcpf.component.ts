import { Component, OnInit } from '@angular/core';
import { ApiService } from '../providers/api.service';

@Component({
  selector: 'app-nfe-cnpjcpf',
  templateUrl: './nfe-cnpjcpf.component.html',
  styleUrls: ['./nfe-cnpjcpf.component.css']
})
export class NfeCnpjcpfComponent implements OnInit {

  public dismissibleError = true;
  public dismissibleKey = false;
  public result = {
    Data: '',
    Tipo: '',
    CnpjCp: 0,
    Numero: 0,
    Serie: 0,
    Modelo: 0,
    Chave: '',
    ValorTotal: 0,
    ValorProd: 0,
    ValorICMS: 0,
    ValorIPI: 0,
    Status: ''
  };
  public table = true;
  constructor(private _apiService: ApiService) { }

  ngOnInit() {
  }

  pesquisar(key) {
    if (key.length === 0) {
      this.dismissibleKey = true;
    } else {
      this.dismissibleKey = false;
      this._apiService.getByCnpjCpf(key).subscribe((response) => {
        if (response.length === 0) {
          this.dismissibleError = false;
          this.table = true;
        } else {
          this.dismissibleError = true;
          this.result = response.data[0];
          this.table = false;
        }
      });
    }
  }
}
