import { NfeStatusComponent } from './nfe-status/nfe-status.component';
import { ChartsModule } from 'ng2-charts';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';


import { TabsModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { Topico1Component } from './topico1/topico1.component';
import { Topico2Component } from './topico2/topico2.component';
import { NfetranErrorComponent } from './nfetran-error/nfetran-error.component';
import { ApiService } from './providers/api.service';
import { HttpClientModule } from '@angular/common/http';
import { NfetranSefazComponent } from './nfetran-sefaz/nfetran-sefaz.component';
import { NfetranSuccessComponent } from './nfetran-success/nfetran-success.component';
import { NfeKeyComponent } from './nfe-key/nfe-key.component';
import { MoedaPipe } from './pipes/moeda.pipe';
import { NfeCnpjcpfComponent } from './nfe-cnpjcpf/nfe-cnpjcpf.component';
import { NfeFiscaltransComponent } from './nfe-fiscaltrans/nfe-fiscaltrans.component';
import { RegexStringPipe } from './pipes/regex-string.pipe';
import { DataTypeComponent } from './data-type/data-type.component';
import { FormsModule } from '@angular/forms';
import { LimitToPipe } from './limit-to.pipe';
import { NfeTotalComponent } from './nfe-total/nfe-total.component';


@NgModule({
  declarations: [
    AppComponent,
    Topico1Component,
    Topico2Component,
    NfetranErrorComponent,
    NfetranSefazComponent,
    NfetranSuccessComponent,
    NfeKeyComponent,
    MoedaPipe,
    NfeCnpjcpfComponent,
    NfeFiscaltransComponent,
    RegexStringPipe,
    DataTypeComponent,
    LimitToPipe,
    NfeTotalComponent,
    NfeStatusComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, TabsModule.forRoot(),
    ChartsModule, AlertModule.forRoot(), FormsModule, TooltipModule.forRoot(), MultiselectDropdownModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
