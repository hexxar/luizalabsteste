import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'regexString'
})
export class RegexStringPipe implements PipeTransform {

  transform(value: String, args?: any): any {
    let newValue = value.replace(/\n/g, '          ');
    return `${newValue}`;
  }

}
