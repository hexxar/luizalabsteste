import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moeda'
})
export class MoedaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let newValue = value.toString();
    newValue = newValue.replace('.', ',');
    return `${newValue}`;
  }

}

