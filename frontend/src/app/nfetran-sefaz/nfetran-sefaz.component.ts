import { Component, OnInit } from '@angular/core';
import { ApiService } from '../providers/api.service';

@Component({
  selector: 'app-nfetran-sefaz',
  templateUrl: './nfetran-sefaz.component.html',
  styleUrls: ['./nfetran-sefaz.component.css']
})
export class NfetranSefazComponent implements OnInit {

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [{ data: [], label: '' }, { data: [], label: '' }];

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }

  constructor(private _apiService: ApiService) { }

  async ngOnInit() {
    await this._apiService.getSefaz().subscribe((response) => {
      const sefaz = response.result.data.map(a => a.key);
      for (let index = 0; index < sefaz.length; index++) {
        this.barChartLabels.push(sefaz[index]);
      }
      this.barChartData = [{ data: response.result.data.map(a => a.error), label: 'Erros' },
      { data: response.result.data.map(a => a.success), label: 'Sucessos' }];

    });
  }
}

