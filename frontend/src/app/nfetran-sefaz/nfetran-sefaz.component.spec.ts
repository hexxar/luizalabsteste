import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfetranSefazComponent } from './nfetran-sefaz.component';

describe('NfetranSefazComponent', () => {
  let component: NfetranSefazComponent;
  let fixture: ComponentFixture<NfetranSefazComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfetranSefazComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfetranSefazComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
