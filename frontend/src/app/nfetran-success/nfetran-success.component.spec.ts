import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfetranSuccessComponent } from './nfetran-success.component';

describe('NfetranSuccessComponent', () => {
  let component: NfetranSuccessComponent;
  let fixture: ComponentFixture<NfetranSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfetranSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfetranSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
