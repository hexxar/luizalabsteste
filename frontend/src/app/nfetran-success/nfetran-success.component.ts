import { Component, OnInit } from '@angular/core';
import { ApiService } from '../providers/api.service';

@Component({
  selector: 'app-nfetran-success',
  templateUrl: './nfetran-success.component.html',
  styleUrls: ['./nfetran-success.component.css']
})
export class NfetranSuccessComponent implements OnInit {

  public barChartOptions: any = {
    scaleShowVerticalLines: true,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [{ data: [], label: '' }];
  public barChartColors: Array<any> = [{
    backgroundColor: ['green']
  }];
  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }

  constructor(private _apiService: ApiService) { }

  async ngOnInit() {
    await this._apiService.getSuccess().subscribe((response) => {
      const sefaz = response.result.data.map(a => a.key);
      for (let index = 0; index < sefaz.length; index++) {
        this.barChartLabels.push(sefaz[index]);
      }
      this.barChartData = [{ data: response.result.data.map(a => a.value), label: 'Sucessos' }];

    });
  }
}

