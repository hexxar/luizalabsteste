import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfeFiscaltransComponent } from './nfe-fiscaltrans.component';

describe('NfeFiscaltransComponent', () => {
  let component: NfeFiscaltransComponent;
  let fixture: ComponentFixture<NfeFiscaltransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfeFiscaltransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfeFiscaltransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
