import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Topico1Component } from './topico1.component';

describe('Topico1Component', () => {
  let component: Topico1Component;
  let fixture: ComponentFixture<Topico1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Topico1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Topico1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
