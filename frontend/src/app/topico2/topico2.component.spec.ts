import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Topico2Component } from './topico2.component';

describe('Topico2Component', () => {
  let component: Topico2Component;
  let fixture: ComponentFixture<Topico2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Topico2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Topico2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
