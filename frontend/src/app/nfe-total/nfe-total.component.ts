import { Component, OnInit } from '@angular/core';
import { ApiService } from '../providers/api.service';

@Component({
  selector: 'app-nfe-total',
  templateUrl: './nfe-total.component.html',
  styleUrls: ['./nfe-total.component.css']
})
export class NfeTotalComponent implements OnInit {
  public dismissibleError = true;
  public dismissibleKey = false;
  public result = {
    Valor_Total: 0,
    Valor_Prod: 0,
    Valor_ICMS: 0,
    Valor_IPI: 0
  };
  public table = true;
  constructor(private _apiService: ApiService) { }
  public type: any = '';
  public end: Date;
  public initial: Date;

  ngOnInit() {
  }

  pesquisar() {
    if (this.initial === null || this.end === null || this.type.length === 0) {
      this.dismissibleKey = true;
    } else {
      this.dismissibleKey = false;
      const initial = `${this.initial.toString().substring(8, 10)}/${this.initial.toString().substring(5, 7)}/${this.initial.toString().substring(0, 4)}`;
      const end = `${this.end.toString().substring(8, 10)}/${this.end.toString().substring(5, 7)}/${this.end.toString().substring(0, 4)}`;

      this._apiService.getTotal(initial, end, this.type).subscribe((response) => {
        if (response.length === 0) {
          this.dismissibleError = false;
          this.table = true;
        } else {
          this.dismissibleError = true;
          this.result = response.data;
          this.table = false;
        }
      });
    }

  }
}
