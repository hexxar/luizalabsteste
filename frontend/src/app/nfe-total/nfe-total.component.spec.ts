import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfeTotalComponent } from './nfe-total.component';

describe('NfeTotalComponent', () => {
  let component: NfeTotalComponent;
  let fixture: ComponentFixture<NfeTotalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfeTotalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfeTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
