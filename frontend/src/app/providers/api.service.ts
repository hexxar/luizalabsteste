import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ApiService {
  private _apiUrl = environment.apiURL;
  private _routeNFeTran = 'nfetran';
  private _routeNFe = 'nfe';

  constructor(private _http: HttpClient) {

  }
  getError(): Observable<any> {
    return this._http.get<any>(`${this._apiUrl}${this._routeNFeTran}/error`);
  }
  getSefaz(): Observable<any> {
    return this._http.get<any>(`${this._apiUrl}${this._routeNFeTran}/sefaz`);
  }
  getSuccess(): Observable<any> {
    return this._http.get<any>(`${this._apiUrl}${this._routeNFeTran}/success`);
  }
  getByKey(key): Observable<any> {
    return this._http.post<any>(`${this._apiUrl}${this._routeNFe}/key`, { key: `${key}` });
  }
  getByCnpjCpf(key): Observable<any> {
    return this._http.post<any>(`${this._apiUrl}${this._routeNFe}/cnpjcpf`, { CnpjCpf: `${key}` });
  }
  getByFiscalTrans(key): Observable<any> {
    return this._http.post<any>(`${this._apiUrl}${this._routeNFe}/fiscaltrans`, { key: `${key}` });
  }
  getByDataType(initial, end, type): Observable<any> {
    return this._http.post<any>(`${this._apiUrl}${this._routeNFe}/datatype`, { initial: `${initial}`, end: `${end}`, type: `${type}` });
  }
  getTotal(initial, end, type): Observable<any> {
    return this._http.post<any>(`${this._apiUrl}${this._routeNFe}/total`, { initial: `${initial}`, end: `${end}`, type: `${type}` });
  }
  getByStatus(status) {
    return this._http.post<any>(`${this._apiUrl}${this._routeNFe}/status`, { status: status });
  }

}

