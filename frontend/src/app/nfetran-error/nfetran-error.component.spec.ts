import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfetranErrorComponent } from './nfetran-error.component';

describe('NfetranErrorComponent', () => {
  let component: NfetranErrorComponent;
  let fixture: ComponentFixture<NfetranErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfetranErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfetranErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
