import { Component, OnInit, Pipe, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../providers/api.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Component({
  selector: 'app-nfetran-error',
  templateUrl: './nfetran-error.component.html',
  styleUrls: ['./nfetran-error.component.css']
})


export class NfetranErrorComponent implements OnInit {
  public pieChartLabels: string[] = [];
  public pieChartData: number[] = [];
  public pieChartType = 'pie';
  public pieChartColors: Array<any> = [{
    backgroundColor: []
  }];
  public erros = '';
  public pieOptions: any = {
    legend: { position: 'left' }
  };
  constructor(private _apiService: ApiService) { }

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }

  async ngOnInit() {

    await this._apiService.getError().subscribe((response) => {
      this.erros = response.result.length;
      const names = response.result.data.map(a => a.key);
      const numbers = response.result.data.map(a => a.value);
      for (let index = 0; index < names.length; index++) {
        this.pieChartLabels.push(names[index]);
        this.pieChartData.push(numbers[index]);

        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        this.pieChartColors[0].backgroundColor[index] = (color);
      }
    });

  }
}
