import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NfeKeyComponent } from './nfe-key.component';

describe('NfeKeyComponent', () => {
  let component: NfeKeyComponent;
  let fixture: ComponentFixture<NfeKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfeKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NfeKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
